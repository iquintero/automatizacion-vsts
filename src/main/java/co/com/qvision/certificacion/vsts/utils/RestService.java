package co.com.qvision.certificacion.vsts.utils;

public enum RestService {

    BASE_URL("https://dev.azure.com/"),
    API_VERSION("?api-version=5.1")
    ;

    private String url;

    RestService(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return url;
    }
}
