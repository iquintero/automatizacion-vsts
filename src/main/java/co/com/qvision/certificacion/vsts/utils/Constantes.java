package co.com.qvision.certificacion.vsts.utils;

public enum Constantes {

    ORGANIZACION("Vicepresidencia%20Servicios%20de%20Tecnología"),
    PROYECTO("GrupoBancolombia");


    private String constante;

    Constantes(String constante) {
        this.constante = constante;
    }

    @Override
    public String toString() {
        return constante;
    }
}
