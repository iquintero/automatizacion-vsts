package co.com.qvision.certificacion.vsts.exceptions;

public class FalloAlVerificarCamposException extends AssertionError {

    public static final String FALLO_VERIFICAR_CAMPOS = "El resultado obtenido no es igual al resultado esperado";

    public FalloAlVerificarCamposException(String message, Throwable cause) {
        super(message, cause);
    }

}