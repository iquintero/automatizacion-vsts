package co.com.qvision.certificacion.vsts.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;

import static co.com.qvision.certificacion.vsts.utils.RestService.API_VERSION;
import static co.com.qvision.certificacion.vsts.utils.RestService.BASE_URL;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class EnviarPeticion implements Task {

    private String id;

    public EnviarPeticion(String id) {
        this.id = id;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Get.resource("{organization}/{project}/_apis/wit/workitems/{id}?api-version=5.1")
                        .with(request -> request.pathParam("organization", "GrupoBancolombia"))
                        .with(request -> request.pathParam("project", "Vicepresidencia Servicios de Tecnología"))
                        .with(request -> request.pathParam("id", id))
                        .with(request -> request.header("Authorization", "Basic OjV6dGR4YWJjY2ZyaTN3ZWZxcDVheHFyNG43d2dvc3diZnF5dWh2a3lidjZnbmVqanRiN2E="))
        );
    }

    public static EnviarPeticion respuesta(String id) {
        return instrumented(EnviarPeticion.class, id);
    }
}
