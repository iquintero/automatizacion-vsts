package co.com.qvision.certificacion.vsts.stepdefinitions;

import co.com.qvision.certificacion.vsts.tasks.EnviarPeticion;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.json.JSONObject;

import static co.com.qvision.certificacion.vsts.utils.RestService.BASE_URL;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

public class VerificarDODStepDefinitions {

    @Before
    public void prepareStage() {
        OnStage.setTheStage(new OnlineCast());
        theActorCalled("Isaac").whoCan(CallAnApi.at(BASE_URL.toString()));
    }

    @Cuando("^verifico en la Vicepresidencia de servicios en el Grupo Bancolombia el DOD con (.*)$")
    public void verificoEnLaVicepresidenciaDeServiciosEnElGrupoBancolombiaElDODConId(String id) {
        theActorInTheSpotlight().attemptsTo(
                EnviarPeticion.respuesta(id)
        );
        JSONObject jsonObject = new JSONObject(SerenityRest.lastResponse().getBody().prettyPrint());
        System.out.println(jsonObject.getJSONObject("fields").get("System.AreaPath"));
    }

    @Entonces("^deberia ver la respuesta exitosa$")
    public void deberiaVerLaRespuestaExitosa() {
        theActorInTheSpotlight().should(
                seeThatResponse("La respuesta debe ser exitosa",
                        response -> response.statusCode(200)));
    }


}
