package co.com.qvision.certificacion.vsts.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@CucumberOptions(
        features = "src/test/resources/features/verificar_dod.feature",
        glue = "co.com.qvision.certificacion.vsts.stepdefinitions",
        snippets = SnippetType.CAMELCASE
)

@RunWith(CucumberWithSerenity.class)
public class VerificarDOD {
}
